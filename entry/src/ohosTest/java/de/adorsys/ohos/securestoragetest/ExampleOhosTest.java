/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package de.adorsys.ohos.securestoragetest;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * ExampleOhosTest
 *
 * @since 2021-05-19
 */
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("de.adorsys.ohos.securestoragetest", actualBundleName);
    }

    /**
     * 初始化button的test
     */
    @Test
    public void initButtonMethod(){
        try {
            Class mainAbilitySlice = Class.forName("de.adorsys.ohos.securestoragetest.slice.MainAbilitySlice");
            Method initButton = mainAbilitySlice.getMethod("initButton");
            Object object = mainAbilitySlice.getConstructor().newInstance();
            initButton.invoke(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据key存储value的test
     */
    @Test
    public void storeMethod(){
        try {
            Class mainAbilitySlice = Class.forName("com.de.adorsys.ohos.securestoragetest.slice.MainAbilitySlice");
            Method store = mainAbilitySlice.getMethod("store");
            Object object = mainAbilitySlice.getConstructor().newInstance();
            store.invoke(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据key查询value的test
     */
    @Test
    public void getMethod(){
        try {
            Class mainAbilitySlice = Class.forName("com.de.adorsys.ohos.securestoragetest.slice.MainAbilitySlice");
            Method get = mainAbilitySlice.getMethod("get");
            Object object = mainAbilitySlice.getConstructor().newInstance();
            get.invoke(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据key删除value的test
     */
    @Test
    public void removeMethod(){
        try {
            Class mainAbilitySlice = Class.forName("com.de.adorsys.ohos.securestoragetest.slice.MainAbilitySlice");
            Method remove = mainAbilitySlice.getMethod("remove");
            Object object = mainAbilitySlice.getConstructor().newInstance();
            remove.invoke(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * 回车切换到下一个TextField的test
     */
    @Test
    public void checkFocusMethod(){
        try {
            Class mainAbilitySlice = Class.forName("com.de.adorsys.ohos.securestoragetest.slice.MainAbilitySlice");
            Method checkFocus = mainAbilitySlice.getMethod("checkFocus");
            Object object = mainAbilitySlice.getConstructor().newInstance();
            checkFocus.invoke(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}