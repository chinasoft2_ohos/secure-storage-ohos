/*
 * Copyright (C) 2017 adorsys GmbH & Co. KG
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.adorsys.ohos.securestoragelibrary;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.HashSet;
import java.util.Set;

import static de.adorsys.ohos.securestoragelibrary.SecureStorageException.ExceptionType.CRYPTO_EXCEPTION;


/**
 * Handles every use case for the developer using Secure Storage.
 * Encryption, Decryption, Storage, Removal etc.
 */
public final class SecurePreferences {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x000110, "SecurePreferences");
    private static final String KEY_SET_COUNT_POSTFIX = "_count";
    private static final String PREFERENCES_NAME = "my_sharePreferences";

    private SecurePreferences() {
    }

    /**
     * Takes plain string value, encrypts it and stores it encrypted in the SecureStorage on the OpenHarmony Device
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @param value   Plain String value that will be encrypted and stored in the SecureStorage
     * @throws SecureStorageException
     */
    public static void setValue(Context context, String key, String value) throws SecureStorageException {
        Context applicationContext = context.getApplicationContext();

        if (!KeystoreTool.keyPairExists(context)) {
            KeystoreTool.generateKeyPair(applicationContext);
        }

        String transformedValue = KeystoreTool.encryptMessage(applicationContext, value);
        if (transformedValue.isEmpty()) {
            HiLog.error(LABEL, "KeystoreTool.keyPairExists : " + KeystoreTool.keyPairExists(context));
            throw new SecureStorageException("Problem during Encryption", null, CRYPTO_EXCEPTION);
        } else {
            setSecureValue(context, key, transformedValue);
        }
    }

    /**
     * Takes plain string value, encrypts it and stores it encrypted in the SecureStorage on the OpenHarmony Device
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @param value   Plain boolean value that will be encrypted and stored in the SecureStorage
     * @throws SecureStorageException
     */
    public static void setValue(Context context, String key, boolean value) throws SecureStorageException {
        setValue(context, key, String.valueOf(value));
    }

    /**
     * Takes plain string value, encrypts it and stores it encrypted in the SecureStorage on the OpenHarmony Device
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @param value   Plain float value that will be encrypted and stored in the SecureStorage
     * @throws SecureStorageException
     */
    public static void setValue(Context context, String key, float value) throws SecureStorageException {
        setValue(context, key, String.valueOf(value));
    }

    /**
     * Takes plain string value, encrypts it and stores it encrypted in the SecureStorage on the OpenHarmony Device
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @param value   Plain long value that will be encrypted and stored in the SecureStorage
     * @throws SecureStorageException
     */
    public static void setValue(Context context, String key, long value) throws SecureStorageException {
        setValue(context, key, String.valueOf(value));
    }

    /**
     * Takes plain string value, encrypts it and stores it encrypted in the SecureStorage on the OpenHarmony Device
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @param value   Plain int value that will be encrypted and stored in the SecureStorage
     * @throws SecureStorageException
     */
    public static void setValue(Context context, String key, int value) throws SecureStorageException {
        setValue(context, key, String.valueOf(value));
    }

    /**
     * Takes plain string value, encrypts it and stores it encrypted in the SecureStorage on the OpenHarmony Device
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @param value   Plain Set(type: String) value that will be encrypted and stored in the SecureStorage
     * @throws SecureStorageException
     */
    public static void setValue(Context context, String key, Set<String> value) throws SecureStorageException {
        setValue(context, key + KEY_SET_COUNT_POSTFIX, String.valueOf(value.size()));
        int i = 0;
        for (String s : value) {
            setValue(context, key + "_" + (i++), s);
        }
    }

    /**
     * Gets encrypted String value for given key from the SecureStorage on the OpenHarmony Device, decrypts it and returns it
     *
     * @param context  Context is used internally
     * @param key      Key used to identify the stored value in SecureStorage
     * @param defValue Default String value that will be returned if the value with given key doesn't exist or an exception is thrown
     * @return Decrypted String value associated with given key from SecureStorage
     */
    public static String getStringValue(Context context, String key, String defValue) {
        Context applicationContext = context.getApplicationContext();
        String result = getSecureValue(applicationContext, key);
        HiLog.info(LABEL, "result 根据key查询结果 : " + key + " value: " + result);
        try {
            if (!result.isEmpty()) {
                String decryptMessage = KeystoreTool.decryptMessage(applicationContext, result);
                return decryptMessage;
            } else {
                return defValue;
            }
        } catch (Exception e) {
            return defValue;
        }
    }

    /**
     * Gets encrypted boolean value for given key from the SecureStorage on the OpenHarmony Device, decrypts it and returns it
     *
     * @param context  Context is used internally
     * @param key      Key used to identify the stored value in SecureStorage
     * @param defValue Default boolean value that will be returned if the value with given key doesn't exist or an exception is thrown
     * @return Decrypted boolean value associated with given key from SecureStorage
     */
    public static boolean getBooleanValue(Context context, String key, boolean defValue) {
        return Boolean.parseBoolean(getStringValue(context, key, String.valueOf(defValue)));
    }

    /**
     * Gets encrypted float value for given key from the SecureStorage on the OpenHarmony Device, decrypts it and returns it
     *
     * @param context  Context is used internally
     * @param key      Key used to identify the stored value in SecureStorage
     * @param defValue Default float value that will be returned if the value with given key doesn't exist or an exception is thrown
     * @return Decrypted float value associated with given key from SecureStorage
     */
    public static float getFloatValue(Context context, String key, float defValue) {
        return Float.parseFloat(getStringValue(context, key, String.valueOf(defValue)));
    }

    /**
     * Gets encrypted long value for given key from the SecureStorage on the OpenHarmony Device, decrypts it and returns it
     *
     * @param context  Context is used internally
     * @param key      Key used to identify the stored value in SecureStorage
     * @param defValue Default long value that will be returned if the value with given key doesn't exist or an exception is thrown
     * @return Decrypted long value associated with given key from SecureStorage
     */
    public static long getLongValue(Context context, String key, long defValue) {
        return Long.parseLong(getStringValue(context, key, String.valueOf(defValue)));
    }

    /**
     * Gets encrypted int value for given key  from the SecureStorage on the OpenHarmony Device, decrypts it and returns it
     *
     * @param context  Context is used internally
     * @param key      Key used to identify the stored value in SecureStorage
     * @param defValue Default int value that will be returned if the value with given key doesn't exist or an exception is thrown
     * @return Decrypted int value associated with given key from SecureStorage
     */
    public static int getIntValue(Context context, String key, int defValue) {
        return Integer.parseInt(getStringValue(context, key, String.valueOf(defValue)));
    }

    /**
     * Gets encrypted int value for given key  from the SecureStorage on the OpenHarmony Device, decrypts it and returns it
     *
     * @param context  Context is used internally
     * @param key      Key used to identify the stored value in SecureStorage
     * @param defValue Default Set(type: String) value that will be returned if the value with given key doesn't exist or an exception is thrown
     * @return Decrypted Set(type: String) value associated with given key from SecureStorage
     */
    public static Set<String> getStringSetValue(Context context, String key, Set<String> defValue) {
        int size = getIntValue(context, key + KEY_SET_COUNT_POSTFIX, -1);
        if (size == -1) {
            return defValue;
        }
        Set<String> res = new HashSet<>(size);
        for (int i = 0; i < size; i++) {
            res.add(getStringValue(context, key + "_" + i, ""));
        }
        return res;
    }

    /**
     * Checks if SecureStorage contains a value for the given key (Does not return the value or check what type it is)
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     * @return True if value exists in SecureStorage, otherwise false
     */
    public static boolean contains(Context context, String key) {
        Context applicationContext = context.getApplicationContext();
        DatabaseHelper databaseHelper = new DatabaseHelper(applicationContext);
        Preferences preferences = databaseHelper.getPreferences(PREFERENCES_NAME);
        return preferences.hasKey(key) && KeystoreTool.keyPairExists(context);
    }

    /**
     * Removes the value for a given key from SecureStorage
     *
     * @param context Context is used internally
     * @param key     Key used to identify the stored value in SecureStorage
     */
    public static void removeValue( Context context,  String key) {
        Context applicationContext = context.getApplicationContext();
        removeSecureValue(applicationContext, key);
    }

    /**
     * Clears all values from the SecureStorage on the OpenHarmony Device and deletes the en/decryption keys
     * Means new keys/keypairs have to be generated for the library to be able to work
     *
     * @param context Context is used internally
     * @throws SecureStorageException
     */
    public static void clearAllValues( Context context) throws SecureStorageException {
        Context applicationContext = context.getApplicationContext();
        if (KeystoreTool.keyPairExists(context)) {
            KeystoreTool.deleteKeyPair(applicationContext);
        }
        clearAllSecureValues(applicationContext);
    }

    /**
     * Registers SecureStorageChangeListener to listen to any changes in SecureStorage
     *
     * @param context  Context is used internally
     * @param listener Provided listener with given behaviour from the developer that will be registered
     */
    public static void registerOnSharedPreferenceChangeListener( Context context,
            Preferences.PreferencesObserver listener) {
        Context applicationContext = context.getApplicationContext();
        DatabaseHelper databaseHelper = new DatabaseHelper(applicationContext);
        Preferences preferences  = databaseHelper.getPreferences(PREFERENCES_NAME);
        preferences.registerObserver(listener);
    }

    /**
     * Unregisters SecureStorageChangeListener from SecureStorage
     *
     * @param context  Context is used internally
     * @param listener Provided listener with given behaviour from the developer that will be unregistered
     */
    public static void unregisterOnSharedPreferenceChangeListener(Context context,
            Preferences.PreferencesObserver listener) {
        Context applicationContext = context.getApplicationContext();
        DatabaseHelper databaseHelper = new DatabaseHelper(applicationContext);
        Preferences preferences = databaseHelper.getPreferences(PREFERENCES_NAME);
        preferences.unregisterObserver(listener);
    }

    private static void setSecureValue(Context context, String key, String value) {
        PreferenceUtils.putString(context, key, value);
    }

    private static String getSecureValue( Context context,  String key) {
        return PreferenceUtils.getString(context, key);
    }

    private static void removeSecureValue( Context context,  String key) {
        PreferenceUtils.deleteString(context, key);
    }

    private static void clearAllSecureValues( Context context) {
        Context applicationContext = context.getApplicationContext();
        DatabaseHelper databaseHelper = new DatabaseHelper(applicationContext);
        Preferences preferences  = databaseHelper.getPreferences(PREFERENCES_NAME);
        preferences.clear();
    }
}