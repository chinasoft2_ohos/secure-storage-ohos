# secure-storage-ohos
### 项目介绍
- 项目名称：secure-storage-ohos工具类
- 所属系列：openharmony第三方组件适配移植
- 功能：RSA非对称加密
- 项目移植状态：移植完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本: Release 0.0.2

### 效果演示

![0](/gif/0.gif)
    
### 安装教程

1. 在项目根目录下的build.gradle文件中添加。

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2. 在entry模块下的build.gradle文件中添加依赖。

```
dependencies {
    implementation('com.gitee.chinasoft_ohos:secure-storage-ohos:1.0.0')
}
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

### 使用说明
secure-storage

1. 布局文件定义，提供控件：secure-storage

```
<?xml version="1.0" encoding="utf-8"?>
<ScrollView
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical">
    <DirectionalLayout
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:left_padding="$float:left_padding"
        ohos:right_padding="$float:right_padding"
        >

        <DirectionalLayout
            ohos:id="$+id:store"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:top_margin="$float:top_padding"
            ohos:orientation="horizontal"
            ohos:alignment="vertical_center"
            >
            <Text
                ohos:id="$+id:store_data"
                ohos:height="match_content"
                ohos:width="match_content"
                ohos:text="Store Date"
                ohos:text_color="$color:green"
                ohos:text_size="$float:text_size"
                />
            <Component
                ohos:height="$float:line_height"
                ohos:width="match_content"
                ohos:layout_alignment="center"
                ohos:start_margin="$float:start_margin"
                ohos:end_margin="$float:end_margin"
                ohos:background_element="$color:green"
                />
        </DirectionalLayout>
        <TextField
            ohos:id="$+id:store_data_key"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:basement="#9e9e9e"
            ohos:focus_border_width="1px"
            ohos:hint="$string:key"
            ohos:text_size="$float:text_size"
            ohos:padding="$float:padding"
            />
        <TextField
            ohos:id="$+id:store_data_value"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:basement="#9e9e9e"
            ohos:focus_border_width="1px"
            ohos:hint="$string:value"
            ohos:text_size="$float:text_size"
            ohos:padding="$float:padding"
            />
        <Button
            ohos:id="$+id:store_button"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:layout_alignment="horizontal_center"
            ohos:padding="$float:padding"
            ohos:top_margin="50"
            ohos:text="STORE"
            ohos:text_color="$color:white"
            ohos:text_size="$float:text_size"
            ohos:background_element="$graphic:button_them"
            />
        <Text
            ohos:id="$+id:storeTextTips"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:top_margin="30"
            ohos:visibility="invisible"
            ohos:text_alignment="center"
            ohos:text="hahahhaha"
            ohos:text_size="$float:text_size"
            ohos:text_color="$color:green"
            />

        <DirectionalLayout
            ohos:id="$+id:get_data_layout"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:orientation="horizontal"
            ohos:alignment="vertical_center"
            ohos:top_margin="30"
            >
            <Text
                ohos:id="$+id:get_data"
                ohos:height="match_content"
                ohos:width="match_content"
                ohos:text="Get Date"
                ohos:text_color="$color:green"
                ohos:text_size="$float:text_size"
                />
            <Component
                ohos:height="$float:line_height"
                ohos:width="match_content"
                ohos:layout_alignment="center"
                ohos:start_margin="$float:start_margin"
                ohos:end_margin="$float:end_margin"
                ohos:background_element="$color:green"
                />
        </DirectionalLayout>

        <TextField
            ohos:id="$+id:get_data_key"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:basement="#9e9e9e"
            ohos:focus_border_width="1px"
            ohos:hint="$string:key"
            ohos:text_size="$float:text_size"
            ohos:padding="$float:padding"
            />
        <Button
            ohos:id="$+id:get_button"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:layout_alignment="horizontal_center"
            ohos:padding="$float:padding"
            ohos:top_margin="50"
            ohos:text="GET"
            ohos:text_color="$color:white"
            ohos:text_size="$float:text_size"
            ohos:background_element="$graphic:button_them"
            />

        <Text
            ohos:id="$+id:getTextTips"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:top_margin="30"
            ohos:visibility="invisible"
            ohos:text_alignment="center"
            ohos:text="hahahhaha"
            ohos:text_size="$float:text_size"
            ohos:text_color="$color:green"
            />

        <DirectionalLayout
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:orientation="horizontal"
            ohos:alignment="vertical_center"
            ohos:top_margin="30"
            >
            <Text
                ohos:id="$+id:remove_data"
                ohos:height="match_content"
                ohos:width="match_content"
                ohos:text="Get Data"
                ohos:text_color="$color:green"
                ohos:text_size="$float:text_size"
                />
            <Component
                ohos:height="$float:line_height"
                ohos:width="match_content"
                ohos:layout_alignment="center"
                ohos:start_margin="$float:start_margin"
                ohos:end_margin="$float:end_margin"
                ohos:background_element="$color:green"
                />
        </DirectionalLayout>

        <TextField
            ohos:id="$+id:remove_data_key"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:basement="#9e9e9e"
            ohos:focus_border_width="1px"
            ohos:hint="$string:key"
            ohos:text_size="$float:text_size"
            ohos:padding="$float:padding"
            />
        <Button
            ohos:id="$+id:remove_button"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:layout_alignment="horizontal_center"
            ohos:padding="$float:padding"
            ohos:top_margin="50"
            ohos:text="REMOVE"
            ohos:text_color="$color:white"
            ohos:text_size="$float:text_size"
            ohos:background_element="$graphic:button_them"
            />
        <Text
            ohos:id="$+id:removeTextTips"
            ohos:height="match_content"
            ohos:width="match_parent"
            ohos:visibility="invisible"
            ohos:top_margin="30"
            ohos:text_alignment="center"
            ohos:text="hahahhaha"
            ohos:text_size="$float:text_size"
            ohos:text_color="$color:green"
            />

    </DirectionalLayout>
</ScrollView>

```

2.在MainAbilitySlice中添加代码

```

    private Button storeButton;
    private Button getButton;
    private Button removeButton;

    // 输入框的值
    private String strokeKeyText;
    private String storeValueText;
    private String getDataKeyText;
    private String removeDataKeyText;

    private void initButton() {
        storeButton = (Button) findComponentById(ResourceTable.Id_store_button);
        getButton = (Button) findComponentById(ResourceTable.Id_get_button);
        removeButton = (Button) findComponentById(ResourceTable.Id_remove_button);

        storeKey = (TextField) findComponentById(ResourceTable.Id_store_data_key);
        storeValue = (TextField) findComponentById(ResourceTable.Id_store_data_value);
        getDataKey = (TextField) findComponentById(ResourceTable.Id_get_data_key);
        removeDataKey = (TextField) findComponentById(ResourceTable.Id_remove_data_key);

        storeTextTips = (Text) findComponentById(ResourceTable.Id_storeTextTips);
        getTextTips = (Text) findComponentById(ResourceTable.Id_getTextTips);
        removeTextTips = (Text) findComponentById(ResourceTable.Id_removeTextTips);

        storeButton.setClickedListener(this::onClick);
        getButton.setClickedListener(this::onClick);
        removeButton.setClickedListener(this::onClick);
        storeKey.setUserNextFocus(Component.FOCUS_NEXT, ResourceTable.Id_store_data_value);
        storeValue.setUserNextFocus(Component.FOCUS_NEXT, ResourceTable.Id_get_data_key);
        getDataKey.setUserNextFocus(Component.FOCUS_NEXT, ResourceTable.Id_remove_data_key);

        storeKey.setKeyEventListener(this::onKeyEvent);
        storeValue.setKeyEventListener(this::onKeyEvent);
        getDataKey.setKeyEventListener(this::onKeyEvent);
    }

    @Override
    public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
        HiLog.info(LABEL,"按键事件触发");
        if (checkFocus(keyEvent, storeKey)) {
            return true;
        } else if (checkFocus(keyEvent, storeValue)) {
            return true;
        } else if (checkFocus(keyEvent, getDataKey)) {
            return true;
        }
        return false;
    }

    private boolean checkFocus(KeyEvent keyEvent,TextField textField) {
        if (textField.hasFocus()) {
            // 如果不增加该判断则会触发两次
            if (keyEvent.isKeyDown()) {
                if (keyEvent.getKeyCode() == keyEvent.KEY_ENTER) {
                    textField.findRequestNextFocus(Component.FOCUS_NEXT);
                    return true;
                }
            }
        }
        return false;
    }

@Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_store_button:
                store();
                break;
            case ResourceTable.Id_get_button:
                get();
                return;
            case ResourceTable.Id_remove_button:
                remove();
                return;
            default:
        }
    }

    // 保存
    private void store() {
        // 获取key和value
        strokeKeyText = storeKey.getText();
        storeValueText = storeValue.getText();

        // 判空
        if (strokeKeyText.isEmpty() || "".equals(strokeKeyText)
            || storeValueText.isEmpty() || "".equals(storeValueText)) {
            createDialog();
            return;
        }
        try {
            SecurePreferences.setValue(getApplicationContext(), strokeKeyText, storeValueText);
        } catch (SecureStorageException e) {
            HiLog.error(LABEL, e + "");
        }

        // 展示成功信息
        storeTextTips.setText("Value: " + storeValueText + " successfully saved for key: " + strokeKeyText);

        // 显示存储key value提示
        storeTextTips.setVisibility(Text.VISIBLE);

        // 隐藏获key取提示
        getTextTips.setVisibility(Text.INVISIBLE);

        // 隐藏删除提示
        removeTextTips.setVisibility(Text.INVISIBLE);

        // 输入框置空
        storeKey.setText("");
        storeValue.setText("");
    }

    // 根据key获取
    private void get() {
        getDataKeyText = getDataKey.getText();

        if (getDataKeyText.isEmpty() || "".equals(getDataKeyText)) {
            createDialog();
            return;
        }
        String value = SecurePreferences.getStringValue(getApplicationContext(), getDataKeyText, FAILED);
        getTextTips.setText("Decrypted Data for key: " + getDataKeyText + " = " + value);
        getTextTips.setVisibility(Text.VISIBLE);
        removeTextTips.setVisibility(Text.INVISIBLE);
        storeTextTips.setVisibility(Text.INVISIBLE);

        getDataKey.setText("");
    }

    // 根据key删除
    private void remove() {
        removeDataKeyText = removeDataKey.getText();
        if (removeDataKeyText.isEmpty() || "".equals(removeDataKeyText)) {
            createDialog();
            return;
        }
        String value = SecurePreferences.getStringValue(getApplicationContext(), removeDataKeyText, FAILED);
        HiLog.info(LABEL, "removeValue " + value);
        if (FAILED.equals(value)) {
            removeTextTips.setText("Value for key: " + removeDataKey.getText() + " not exist !");
        } else {
            SecurePreferences.removeValue(getApplicationContext(), removeDataKeyText);
            removeTextTips.setText("Value for key: " + removeDataKey.getText() + " successfully deleted");
        }
        removeTextTips.setVisibility(Text.VISIBLE);
        getTextTips.setVisibility(Text.INVISIBLE);
        storeTextTips.setVisibility(Text.INVISIBLE);

        // 输入框置空
        removeDataKey.setText("");
    }

    // 提示弹窗
    private void createDialog() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.GRAY.getValue()));
        shapeElement.setAlpha(ALPHA);
        shapeElement.setCornerRadius(CORNER_RADIUS);

        Text dialog = new Text(getContext());
        dialog.setText("Fields cannot be empty");
        dialog.setTextColor(Color.BLACK);
        dialog.setBackground(shapeElement);

        dialog.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        dialog.setTextSize(TEXT_SIZE, Text.TextSizeType.PX);
        dialog.setPadding(DIALOG_PADDING, DIALOG_PADDING, DIALOG_PADDING, DIALOG_PADDING);
        new ToastDialog(getContext())
            .setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT)
            .setComponent(dialog)
            .setDuration(DURATION_TIME)
            .setCornerRadius(TOAST_DIALOG_RADIUS)
            // Toast显示在界面底部
            .setAlignment(LayoutAlignment.BOTTOM)
            .show();
    }

```

### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

### 版本迭代

- 1.0.0

#### 版权和许可信息 
- Apache License 2.0

